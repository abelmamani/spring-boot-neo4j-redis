package category.usecase.createcategoryusecase;

import category.exceptions.CategoryException;
import category.inputs.CreateCategoryInput;
import category.models.Category;
import category.outputs.CreateCategoryRepository;

public class CreateCategoryUseCase implements CreateCategoryInput {
    private CreateCategoryRepository createCategoryRepository;

    public CreateCategoryUseCase(CreateCategoryRepository createCategoryRepository) {
        this.createCategoryRepository = createCategoryRepository;
    }

    @Override
    public Long createCategory(CreateCategoryRequestModel createCategoryRequestModel) {
        if(createCategoryRepository.existsByName(createCategoryRequestModel.getName()))
            throw new CategoryException("the category already exists");
        Category category = Category.getInstance(null, createCategoryRequestModel.getName(), createCategoryRequestModel.getDescription());
        return createCategoryRepository.save(category);
    }
}
