package category.usecase.createcategoryusecase;

public class CreateCategoryRequestModel {
    private String name;
    private String description;
    private CreateCategoryRequestModel(){}
    private CreateCategoryRequestModel(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public static CreateCategoryRequestModel getInstance(String name, String description) {
        return new CreateCategoryRequestModel(name, description);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
