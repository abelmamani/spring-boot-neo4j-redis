package category.usecase.getcategoriesusecase;

import java.io.Serializable;

public class GetCategeriesResponseModel implements Serializable {
    private Long id;
    private String name;
    private String description;
    public GetCategeriesResponseModel(){}

    private GetCategeriesResponseModel(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
    public static GetCategeriesResponseModel getInstance(Long id, String name, String description) {
        return new GetCategeriesResponseModel(id, name, description);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
