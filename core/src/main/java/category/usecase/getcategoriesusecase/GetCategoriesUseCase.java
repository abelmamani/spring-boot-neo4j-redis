package category.usecase.getcategoriesusecase;

import category.inputs.GetCategoriesInput;
import category.outputs.GetCategoriesRepository;
import java.util.Collection;
import java.util.stream.Collectors;

public class GetCategoriesUseCase implements GetCategoriesInput {
    private GetCategoriesRepository getCategoriesRepository;

    public GetCategoriesUseCase(GetCategoriesRepository getCategoriesRepository) {
        this.getCategoriesRepository = getCategoriesRepository;
    }

    @Override
    public Collection<GetCategeriesResponseModel> getCategories() {
        return getCategoriesRepository.findAll().stream().map(category -> GetCategeriesResponseModel.getInstance(category.getId(), category.getName(), category.getDescription())).collect(Collectors.toList());
    }
}
