package category.inputs;

import category.usecase.createcategoryusecase.CreateCategoryRequestModel;

public interface CreateCategoryInput {
    Long createCategory(CreateCategoryRequestModel createCategoryRequestModel);
}
