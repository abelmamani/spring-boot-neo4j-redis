package category.inputs;
import category.usecase.getcategoriesusecase.GetCategeriesResponseModel;

import java.util.Collection;

public interface GetCategoriesInput {
    Collection<GetCategeriesResponseModel> getCategories();
}
