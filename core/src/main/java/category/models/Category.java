package category.models;

import category.exceptions.CategoryException;

import java.io.Serializable;

public class Category implements Serializable {
    private Long id;
    private String name;
    private String description;

    private Category(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
    public static Category getInstance(Long id, String name, String description) {
        if(name == null || description == null)
            throw new CategoryException("the name or description cannot be null");
        if(name.isBlank())
            throw new CategoryException("the name cannot be empty");
        if(description.isBlank())
            throw new CategoryException("the description cannot be empty");
        return new Category(id, name, description);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
