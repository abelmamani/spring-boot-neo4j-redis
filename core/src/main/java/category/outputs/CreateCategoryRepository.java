package category.outputs;

import category.models.Category;

public interface CreateCategoryRepository {
    boolean existsByName(String name);
    Long save(Category category);
}
