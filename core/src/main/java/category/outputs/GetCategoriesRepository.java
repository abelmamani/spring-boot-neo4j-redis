package category.outputs;

import category.models.Category;

import java.util.Collection;

public interface GetCategoriesRepository {
    Collection<Category> findAll();
}
