package products.models;

import category.models.Category;
import products.exceptions.ProductException;

public class Product {
    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer stock;
    private Category category;

    private Product(Long id, String name, String description, Double price, Integer stock, Category category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.category = category;
    }
    public static Product getInstance(Long id, String name, String description, Double price, Integer stock, Category category) {
        if (name == null)
            throw new ProductException("the name cannot be null");
        if (description == null)
            throw new ProductException("the description cannot be null");
        if (price == null)
            throw new ProductException("the price cannot be null");
        if (price <= 0 || stock < 0)
            throw new ProductException("the price or stock cannot be negative");
        if (stock == null)
            throw new ProductException("the stock cannot be null");
        if (category == null)
            throw new ProductException("the category cannot be null");
        return new Product(id, name, description, price, stock, category);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getStock() {
        return stock;
    }

    public Category getCategory() {
        return category;
    }
}
