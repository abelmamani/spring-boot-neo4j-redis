package ar.edu.undec.adapter.service.categories.config;

import category.inputs.CreateCategoryInput;
import category.inputs.GetCategoriesInput;
import category.outputs.CreateCategoryRepository;
import category.outputs.GetCategoriesRepository;
import category.usecase.getcategoriesusecase.GetCategoriesUseCase;
import category.usecase.createcategoryusecase.CreateCategoryUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CategoryBeanConfig {
    @Bean
    public CreateCategoryInput createCategoryInput(CreateCategoryRepository createCategoryRepository){
        return new CreateCategoryUseCase(createCategoryRepository);
    }

    @Bean
    public GetCategoriesInput getCategoriesInput(GetCategoriesRepository getCategoriesRepository){
        return new GetCategoriesUseCase(getCategoriesRepository);
    }
}
