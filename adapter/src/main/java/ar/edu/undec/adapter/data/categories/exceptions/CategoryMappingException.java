package ar.edu.undec.adapter.data.categories.exceptions;

public class CategoryMappingException extends RuntimeException{
    public CategoryMappingException(String msg) {
        super(msg);
    }
}
