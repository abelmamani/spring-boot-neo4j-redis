package ar.edu.undec.adapter.data.categories.mappers;

import ar.edu.undec.adapter.data.categories.exceptions.CategoryMappingException;
import ar.edu.undec.adapter.data.categories.models.CategoryEntity;
import category.models.Category;

public class CategoryDataMapper {
    public static Category dataCoreMapper(CategoryEntity categoryEntity){
        try {
            return Category.getInstance(categoryEntity.getId(), categoryEntity.getName(), categoryEntity.getDescription());
        }catch (RuntimeException exception){
            throw new CategoryMappingException("error mapping from entity to core");
        }
    }

    public static CategoryEntity dataEntityMapper(Category category){
        try {
            return new CategoryEntity(category.getId(), category.getName(), category.getDescription());
        }catch (RuntimeException exception){
            throw new CategoryMappingException("error mapping from core to entity");
        }
    }
}
