package ar.edu.undec.adapter.service.categories.controller;


import category.inputs.CreateCategoryInput;
import category.inputs.GetCategoriesInput;
import category.usecase.createcategoryusecase.CreateCategoryRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("categories")
@CrossOrigin("*")
public class CategoryController {
    private GetCategoriesInput getCategoriesInput;
    private CreateCategoryInput createCategoryInput;

    @Autowired
    public CategoryController(GetCategoriesInput getCategoriesInput, CreateCategoryInput createCategoryInput) {
        this.getCategoriesInput = getCategoriesInput;
        this.createCategoryInput = createCategoryInput;
    }

    @GetMapping
    public ResponseEntity<?> getCategories(){
        return ResponseEntity.ok(getCategoriesInput.getCategories());

    }
    @PostMapping
    public ResponseEntity<?> createCategory(@RequestBody CreateCategoryRequestModel createCategoryRequestModel){
        try {
            return ResponseEntity.created(null).body(createCategoryInput.createCategory(createCategoryRequestModel));
        }catch (RuntimeException exception){
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }
}
