package ar.edu.undec.adapter.service.categories.mapper;

import ar.edu.undec.adapter.service.categories.exceptions.CategoryDTOMappingException;
import ar.edu.undec.adapter.service.categories.models.CategoryDTO;
import category.models.Category;

public class CategoryMapperDTO {
    public static Category dataCoreMapper(CategoryDTO categoryDTO){
        try {
            return Category.getInstance(categoryDTO.getId(), categoryDTO.getName(), categoryDTO.getDescription());
        }catch (RuntimeException exception){
            throw new CategoryDTOMappingException("error mapping from entity to core");
        }
    }

    public static CategoryDTO dataDTOMapper(Category category){
        try {
            return new CategoryDTO(category.getId(), category.getName(), category.getDescription());
        }catch (RuntimeException exception){
            throw new CategoryDTOMappingException("error mapping from core to entity");
        }
    }
}
