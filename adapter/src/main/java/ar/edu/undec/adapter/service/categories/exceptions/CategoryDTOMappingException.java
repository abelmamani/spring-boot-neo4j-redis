package ar.edu.undec.adapter.service.categories.exceptions;

public class CategoryDTOMappingException extends RuntimeException{
    public CategoryDTOMappingException(String msg) {
        super(msg);
    }
}
