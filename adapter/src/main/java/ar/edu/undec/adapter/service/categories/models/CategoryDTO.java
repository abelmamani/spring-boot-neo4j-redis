package ar.edu.undec.adapter.service.categories.models;

import java.io.Serializable;

public class CategoryDTO implements Serializable {
    private Long id;
    private String name;
    private String description;
    public CategoryDTO(){}

    public CategoryDTO(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
