package ar.edu.undec.adapter.data.categories.crud;

import ar.edu.undec.adapter.data.categories.models.CategoryEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryCRUD extends Neo4jRepository<CategoryEntity, Long> {
    boolean existsByName(String name);
}
