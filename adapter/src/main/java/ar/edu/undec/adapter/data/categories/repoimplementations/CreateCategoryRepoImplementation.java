package ar.edu.undec.adapter.data.categories.repoimplementations;

import ar.edu.undec.adapter.data.categories.crud.CategoryCRUD;
import ar.edu.undec.adapter.data.categories.mappers.CategoryDataMapper;
import category.models.Category;
import category.outputs.CreateCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class CreateCategoryRepoImplementation implements CreateCategoryRepository {
    private CategoryCRUD categoryCRUD;
    @Autowired
    public CreateCategoryRepoImplementation(CategoryCRUD categoryCRUD) {
        this.categoryCRUD = categoryCRUD;
    }

    @Override
    public boolean existsByName(String name) {
        return categoryCRUD.existsByName(name);
    }
    @CacheEvict(value = "categories", allEntries = true)
    @Override
    public Long save(Category category) {
        return categoryCRUD.save(CategoryDataMapper.dataEntityMapper(category)).getId();
    }
}
