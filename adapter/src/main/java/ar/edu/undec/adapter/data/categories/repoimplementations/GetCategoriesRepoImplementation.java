package ar.edu.undec.adapter.data.categories.repoimplementations;

import ar.edu.undec.adapter.data.categories.crud.CategoryCRUD;
import ar.edu.undec.adapter.data.categories.mappers.CategoryDataMapper;
import category.models.Category;
import category.outputs.GetCategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class GetCategoriesRepoImplementation implements GetCategoriesRepository {
    private CategoryCRUD categoryCRUD;
    @Autowired
    public GetCategoriesRepoImplementation(CategoryCRUD categoryCRUD) {
        this.categoryCRUD = categoryCRUD;
    }

    @Cacheable(value = "categories", key = "#root.methodName")
    @Override
    public Collection<Category> findAll() {
        return categoryCRUD.findAll().stream().map(CategoryDataMapper::dataCoreMapper).collect(Collectors.toList());
    }
}
